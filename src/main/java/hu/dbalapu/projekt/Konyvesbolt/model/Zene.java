package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Zene {

    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty hossz = new SimpleIntegerProperty();

    public Zene(int aru_id, int hossz) {
        this.aru_id.set(aru_id);
        this.hossz.set(hossz);
    }

    public int getAru_id() {
        return aru_id.get();
    }


    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getHossz() {
        return hossz.get();
    }

    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty hosszProperty() {
        return hossz;
    }

    public void setHossz(Integer hossz) {
        this.hossz.set(hossz);
    }

    public void setHossz(int hossz) {
        this.hossz.set(hossz);
    }

    public void copyTo(Zene zene) {
        zene.aru_id.set(this.getAru_id());
        zene.hossz.set(this.getHossz());
    }

    public Zene() {
    }
}

package hu.dbalapu.projekt.Konyvesbolt.model;
import javafx.beans.property.*;

public class Mufajok {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty mufaj_neve = new SimpleStringProperty();
    private IntegerProperty szulo_mufaj_id = new SimpleIntegerProperty();
    private IntegerProperty melysegi_szint = new SimpleIntegerProperty();

    public Mufajok(int id, String mufaj_neve, int szulo_mufaj_id, int melysegi_szint) {
        this.id.set(id);
        this.mufaj_neve.set(mufaj_neve);
        this.szulo_mufaj_id.set(szulo_mufaj_id);
        this.melysegi_szint.set(melysegi_szint);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty szulo_mufaj_idProperty() {
        return szulo_mufaj_id;
    }

    public IntegerProperty melysegi_szintProperty() {
        return melysegi_szint;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getMufaj_neve() {
        return mufaj_neve.get();
    }

    public StringProperty mufaj_neveProperty() {
        return mufaj_neve;
    }

    public void setMufaj_neve(String mufaj_neve) {
        this.mufaj_neve.set(mufaj_neve);
    }

    public int getSzulo_mufaj_id() {
        return szulo_mufaj_id.get();
    }

    public void setSzulo_mufaj_id(int szulo_mufaj_id) {
        this.szulo_mufaj_id.set(szulo_mufaj_id);
    }

    public int getMelysegi_szint() {
        return melysegi_szint.get();
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    public void setSzulo_mufaj_id(Integer szulo_mufaj_id) {
        this.szulo_mufaj_id.set(szulo_mufaj_id);
    }

    public void setMelysegi_szint(Integer melysegi_szint) {
        this.melysegi_szint.set(melysegi_szint);
    }

    public void setMelysegi_szint(int melysegi_szint) {
        this.melysegi_szint.set(melysegi_szint);
    }

    public void copyTo(Mufajok mufajok) {
        mufajok.id.set(this.getId());
        mufajok.mufaj_neve.set(this.getMufaj_neve());
        mufajok.szulo_mufaj_id.set(this.getSzulo_mufaj_id());
        mufajok.melysegi_szint.set(this.getMelysegi_szint());
    }

    public Mufajok() {
    }
}

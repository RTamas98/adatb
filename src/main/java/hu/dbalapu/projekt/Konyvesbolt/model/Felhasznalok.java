package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Felhasznalok {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty felhasznalonev = new SimpleStringProperty();
    private StringProperty jelszo = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();
    private StringProperty nev = new SimpleStringProperty();
    private StringProperty telefon = new SimpleStringProperty();
    private StringProperty irsz = new SimpleStringProperty();
    private StringProperty varos = new SimpleStringProperty();
    private StringProperty utca = new SimpleStringProperty();
    private IntegerProperty hazszam = new SimpleIntegerProperty();
    private IntegerProperty jogosultsag = new SimpleIntegerProperty();

    public Felhasznalok(int id, String felhasznalonev, String jelszo, String email, String nev, String telefon, String irsz, String varos, String utca, int hazszam, int jogosultsag) {
        this.id.set(id);
        this.felhasznalonev.set(felhasznalonev);
        this.jelszo.set(jelszo);
        this.email.set(email);
        this.nev.set(nev);
        this.telefon.set(telefon);
        this.irsz.set(irsz);
        this.varos.set(varos);
        this.utca.set(utca);
        this.hazszam.set(hazszam);
        this.jogosultsag.set(jogosultsag);
    }

    public int getId() {
        return id.get();
    }


    public void setId(int id) {
        this.id.set(id);
    }

    public String getFelhasznalonev() {
        return felhasznalonev.get();
    }

    public StringProperty felhasznalonevProperty() {
        return felhasznalonev;
    }

    public void setFelhasznalonev(String felhasznalonev) {
        this.felhasznalonev.set(felhasznalonev);
    }

    public String getJelszo() {
        return jelszo.get();
    }

    public StringProperty jelszoProperty() {
        return jelszo;
    }

    public void setJelszo(String jelszo) {
        this.jelszo.set(jelszo);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getNev() {
        return nev.get();
    }

    public StringProperty nevProperty() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev.set(nev);
    }

    public String getTelefon() {
        return telefon.get();
    }

    public StringProperty telefonProperty() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon.set(telefon);
    }

    public String getIrsz() {
        return irsz.get();
    }

    public StringProperty irszProperty() {
        return irsz;
    }

    public void setIrsz(String irsz) {
        this.irsz.set(irsz);
    }

    public String getVaros() {
        return varos.get();
    }

    public StringProperty varosProperty() {
        return varos;
    }

    public void setVaros(String varos) {
        this.varos.set(varos);
    }

    public String getUtca() {
        return utca.get();
    }

    public StringProperty utcaProperty() {
        return utca;
    }

    public void setUtca(String utca) {
        this.utca.set(utca);
    }

    public int getHazszam() {
        return hazszam.get();
    }

    public void setHazszam(int hazszam) {
        this.hazszam.set(hazszam);
    }

    public int getJogosultsag() {
        return jogosultsag.get();
    }


    public void setId(Integer id) {
        this.id.set(id);
    }

    public void setHazszam(Integer hazszam) {
        this.hazszam.set(hazszam);
    }

    public void setJogosultsag(Integer jogosultsag) {
        this.jogosultsag.set(jogosultsag);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty hazszamProperty() {
        return hazszam;
    }

    public IntegerProperty jogosultsagProperty() {
        return jogosultsag;
    }

    public void setJogosultsag(int jogosultsag) {
        this.jogosultsag.set(jogosultsag);
    }

    public Felhasznalok() {
    }

    public void copyTo(Felhasznalok felhasznalok){
        felhasznalok.id.set(this.getId());
        felhasznalok.felhasznalonev.set(this.getFelhasznalonev());
        felhasznalok.jelszo.set(this.getJelszo());
        felhasznalok.email.set(this.getEmail());
        felhasznalok.nev.set(this.getNev());
        felhasznalok.telefon.set(this.getTelefon());
        felhasznalok.irsz.set(this.getIrsz());
        felhasznalok.varos.set(this.getVaros());
        felhasznalok.utca.set(this.getUtca());
        felhasznalok.hazszam.set(this.getHazszam());
        felhasznalok.jogosultsag.set(this.getJogosultsag());
    }
}

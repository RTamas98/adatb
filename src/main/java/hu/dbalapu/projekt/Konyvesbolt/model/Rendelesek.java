package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Rendelesek {

    private IntegerProperty id = new SimpleIntegerProperty();
    private IntegerProperty felhasznalo_id = new SimpleIntegerProperty();
    private IntegerProperty aruhaz_id = new SimpleIntegerProperty();
    private IntegerProperty kiszallitva = new SimpleIntegerProperty();
    private StringProperty datum = new SimpleStringProperty();

    public Rendelesek(int id, int felhasznalo_id, int aruhaz_id, int kiszallitva, String datum) {
        this.id.set(id);
        this.felhasznalo_id.set(felhasznalo_id);
        this.aruhaz_id.set(aruhaz_id);
        this.kiszallitva.set(kiszallitva);
        this.datum.set(datum);
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getFelhasznalo_id() {
        return felhasznalo_id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty felhasznalo_idProperty() {
        return felhasznalo_id;
    }

    public IntegerProperty aruhaz_idProperty() {
        return aruhaz_id;
    }

    public IntegerProperty kiszallitvaProperty() {
        return kiszallitva;
    }

    public void setFelhasznalo_id(int felhasznalo_id) {
        this.felhasznalo_id.set(felhasznalo_id);
    }

    public int getAruhaz_id() {
        return aruhaz_id.get();
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    public void setFelhasznalo_id(Integer felhasznalo_id) {
        this.felhasznalo_id.set(felhasznalo_id);
    }

    public void setAruhaz_id(Integer aruhaz_id) {
        this.aruhaz_id.set(aruhaz_id);
    }

    public void setKiszallitva(Integer kiszallitva) {
        this.kiszallitva.set(kiszallitva);
    }

    public void setAruhaz_id(int aruhaz_id) {
        this.aruhaz_id.set(aruhaz_id);
    }

    public int getKiszallitva() {
        return kiszallitva.get();
    }

    public void setKiszallitva(int kiszallitva) {
        this.kiszallitva.set(kiszallitva);
    }

    public String getDatum() {
        return datum.get();
    }

    public StringProperty datumProperty() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum.set(datum);
    }

    public void copyTo(Rendelesek rendelesek) {
        rendelesek.id.set(this.getId());
        rendelesek.felhasznalo_id.set(this.getFelhasznalo_id());
        rendelesek.aruhaz_id.set(this.getAruhaz_id());
        rendelesek.kiszallitva.set(this.getKiszallitva());
        rendelesek.datum.set(this.getDatum());
    }

    public Rendelesek() {
    }
}

package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Torzsvasarlok {

    private IntegerProperty felhasznalo_id = new SimpleIntegerProperty();
    private IntegerProperty kedvezmeny = new SimpleIntegerProperty();

    public Torzsvasarlok(int felhasznalo_id, int kedvezmeny) {
        this.felhasznalo_id.set(felhasznalo_id);
        this.kedvezmeny.set(kedvezmeny);
    }

    public int getFelhasznalo_id() {
        return felhasznalo_id.get();
    }

    public IntegerProperty felhasznalo_idProperty() {
        return felhasznalo_id;
    }

    public IntegerProperty kedvezmenyProperty() {
        return kedvezmeny;
    }

    public void setFelhasznalo_id(Integer felhasznalo_id) {
        this.felhasznalo_id.set(felhasznalo_id);
    }


    public void setKedvezmeny(Integer kedvezmeny) {
        this.kedvezmeny.set(kedvezmeny);
    }

    public void setFelhasznalo_id(int felhasznalo_id) {
        this.felhasznalo_id.set(felhasznalo_id);
    }

    public int getKedvezmeny() {
        return kedvezmeny.get();
    }


    public void setKedvezmeny(int kedvezmeny) {
        this.kedvezmeny.set(kedvezmeny);
    }

    public void copyTo(Torzsvasarlok torzsvasarlok) {
        torzsvasarlok.felhasznalo_id.set(this.getFelhasznalo_id());
        torzsvasarlok.kedvezmeny.set(this.getKedvezmeny());
    }

    public Torzsvasarlok() {
    }
}

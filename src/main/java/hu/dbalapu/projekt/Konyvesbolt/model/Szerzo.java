package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Szerzo {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty nev = new SimpleStringProperty();

    public Szerzo(int id, String nev) {
        this.id.set(id);
        this.nev.set(nev);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getNev() {
        return nev.get();
    }

    public StringProperty nevProperty() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev.set(nev);
    }

    public void copyTo(Szerzo szerzo) {
        szerzo.id.set(this.getId());
        szerzo.nev.set(this.getNev());
    }

    public Szerzo() {
    }
}

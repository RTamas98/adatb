package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Kiado {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty nev = new SimpleStringProperty();
    private StringProperty cim = new SimpleStringProperty();

    public Kiado(int id, String nev, String cim) {
        this.id.set(id);
        this.nev.set(nev);
        this.cim.set(cim);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getNev() {
        return nev.get();
    }

    public StringProperty nevProperty() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev.set(nev);
    }

    public String getCim() {
        return cim.get();
    }

    public StringProperty cimProperty() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim.set(cim);
    }

    public void copyTo(Kiado kiado) {
        kiado.cim.set(this.getCim());
        kiado.id.set(this.getId());
        kiado.nev.set(this.getNev());
    }

    public Kiado() {
    }
}

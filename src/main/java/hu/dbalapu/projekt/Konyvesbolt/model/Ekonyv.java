package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Ekonyv {

    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty meret = new SimpleIntegerProperty();
    private IntegerProperty oldalszam = new SimpleIntegerProperty();
    private StringProperty kiterjesztes = new SimpleStringProperty();

    public Ekonyv(int aru_id, int meret, int oldalszam, String kiterjesztes) {
        this.aru_id.set(aru_id);
        this.meret.set(meret);
        this.oldalszam.set(oldalszam);
        this.kiterjesztes.set(kiterjesztes);
    }

    public int getAru_id() {
        return aru_id.get();
    }

    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getMeret() {
        return meret.get();
    }

    public void setMeret(int meret) {
        this.meret.set(meret);
    }

    public int getOldalszam() {
        return oldalszam.get();
    }

    public void setOldalszam(int oldalszam) {
        this.oldalszam.set(oldalszam);
    }

    public String getKiterjesztes() {
        return kiterjesztes.get();
    }

    public StringProperty kiterjesztesProperty() {
        return kiterjesztes;
    }

    public void setKiterjesztes(String kiterjesztes) {
        this.kiterjesztes.set(kiterjesztes);
    }


    public void setAru_id(Integer aru_id) {
        this.aru_id.set(aru_id);
    }

    public void setMeret(Integer meret) {
        this.meret.set(meret);
    }

    public void setOldalszam(Integer oldalszam) {
        this.oldalszam.set(oldalszam);
    }

    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty meretProperty() {
        return meret;
    }

    public IntegerProperty oldalszamProperty() {
        return oldalszam;
    }

    public Ekonyv() {
    }

    public void copyTo(Ekonyv  ekonyv){
        ekonyv.aru_id.set(this.getAru_id());
        ekonyv.meret.set(this.getMeret());
        ekonyv.oldalszam.set(this.getOldalszam());
        ekonyv.kiterjesztes.set(this.getKiterjesztes());
    }
}

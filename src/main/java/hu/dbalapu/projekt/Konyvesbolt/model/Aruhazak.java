package hu.dbalapu.projekt.Konyvesbolt.model;

import javafx.beans.property.*;

public class Aruhazak {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty cim = new SimpleStringProperty();
    private StringProperty telefon = new SimpleStringProperty();

    public Aruhazak(int id, String cim, String telefon) {
        this.id.set(id);
        this.cim.set(cim);
        this.telefon.set(telefon);
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    public Aruhazak() {
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getCim() {
        return cim.get();
    }

    public StringProperty cimProperty() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim.set(cim);
    }

    public String getTelefon() {
        return telefon.get();
    }

    public StringProperty telefonProperty() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon.set(telefon);
    }

    public void copyTo(Aruhazak  aruhazak){
        aruhazak.id.set(this.getId());
        aruhazak.cim.set(this.getCim());
        aruhazak.telefon.set(this.getTelefon());
    }
}

package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Nyilvantartas {

    private IntegerProperty aruhaz_id = new SimpleIntegerProperty();
    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty db = new SimpleIntegerProperty();

    public Nyilvantartas(int aruhaz_id, int aru_id, int db) {
        this.aruhaz_id.set(aruhaz_id);
        this.aru_id.set(aru_id);
        this.db.set(db);
    }

    public int getAruhaz_id() {
        return aruhaz_id.get();
    }

    public IntegerProperty aruhaz_idProperty() {
        return aruhaz_id;
    }

    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty dbProperty() {
        return db;
    }

    public void setAruhaz_id(int aruhaz_id) {
        this.aruhaz_id.set(aruhaz_id);
    }

    public int getAru_id() {
        return aru_id.get();
    }

    public void setAruhaz_id(Integer aruhaz_id) {
        this.aruhaz_id.set(aruhaz_id);
    }

    public void setAru_id(Integer aru_id) {
        this.aru_id.set(aru_id);
    }

    public void setDb(Integer db) {
        this.db.set(db);
    }

    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getDb() {
        return db.get();
    }


    public void setDb(int db) {
        this.db.set(db);
    }

    public void copyTo(Nyilvantartas nyilvantartas) {
        nyilvantartas.aruhaz_id.set(this.getAruhaz_id());
        nyilvantartas.aru_id.set(this.getAru_id());
        nyilvantartas.db.set(this.getDb());
    }

    public Nyilvantartas() {
    }
}

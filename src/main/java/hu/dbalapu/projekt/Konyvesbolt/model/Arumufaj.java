package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Arumufaj {

    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty mufaj_id = new SimpleIntegerProperty();

    public Arumufaj(int aru_id, int mufaj_id) {
        this.aru_id.set(aru_id);
        this.mufaj_id.set(mufaj_id);
    }

    public Arumufaj() {
    }

    public void setAru_id(Integer aru_id) {
        this.aru_id.set(aru_id);
    }

    public void setMufaj_id(Integer mufaj_id) {
        this.mufaj_id.set(mufaj_id);
    }

    public int getAru_id() {
        return aru_id.get();
    }

    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getMufaj_id() {
        return mufaj_id.get();
    }

    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty mufaj_idProperty() {
        return mufaj_id;
    }

    public void setMufaj_id(int mufaj_id) {
        this.mufaj_id.set(mufaj_id);
    }

    public void copyTo(Arumufaj  arumufaj){
        arumufaj.aru_id.set(this.getAru_id());
        arumufaj.mufaj_id.set(this.getMufaj_id());
    }
}

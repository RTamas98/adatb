package hu.dbalapu.projekt.Konyvesbolt.model;

import javafx.beans.property.*;

public class Aruk {

    private IntegerProperty id = new SimpleIntegerProperty();
    private IntegerProperty szerzo_id = new SimpleIntegerProperty();
    private IntegerProperty kiado_id = new SimpleIntegerProperty();
    private IntegerProperty kiadas_eve = new SimpleIntegerProperty();
    private StringProperty leiras = new SimpleStringProperty();
    private StringProperty cim = new SimpleStringProperty();
    private IntegerProperty ar = new SimpleIntegerProperty();
    private IntegerProperty nyelv_id = new SimpleIntegerProperty();

    public Aruk() {
    }

    public Aruk(int id, int szerzo_id, int kiado_id, int kiadas_eve, String leiras, String cim, int ar, int nyelv_id) {
        this.id.set(id);
        this.szerzo_id.set(szerzo_id);
        this.kiado_id.set(kiado_id);
        this.kiadas_eve.set(kiadas_eve);
        this.leiras.set(leiras);
        this.cim.set(cim);
        this.ar.set(ar);
        this.nyelv_id.set(nyelv_id);
    }

    public int getId() {
        return id.get();
    }



    public void setId(int id) {
        this.id.set(id);
    }

    public int getSzerzo_id() {
        return szerzo_id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty szerzo_idProperty() {
        return szerzo_id;
    }

    public IntegerProperty kiado_idProperty() {
        return kiado_id;
    }

    public IntegerProperty kiadas_eveProperty() {
        return kiadas_eve;
    }

    public IntegerProperty arProperty() {
        return ar;
    }

    public IntegerProperty nyelv_idProperty() {
        return nyelv_id;
    }

    public void setNyelv_id(Integer nyelv_id) {
        this.nyelv_id.set(nyelv_id);
    }

    public void setSzerzo_id(int szerzo_id) {
        this.szerzo_id.set(szerzo_id);
    }

    public int getKiado_id() {
        return kiado_id.get();
    }

    public void setKiado_id(int kiado_id) {
        this.kiado_id.set(kiado_id);
    }

    public int getKiadas_eve() {
        return kiadas_eve.get();
    }

    public void setKiadas_eve(int kiadas_eve) {
        this.kiadas_eve.set(kiadas_eve);
    }

    public String getLeiras() {
        return leiras.get();
    }

    public StringProperty leirasProperty() {
        return leiras;
    }

    public void setLeiras(String leiras) {
        this.leiras.set(leiras);
    }

    public String getCim() {
        return cim.get();
    }

    public StringProperty cimProperty() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim.set(cim);
    }

    public int getAr() {
        return ar.get();
    }


    public void setAr(int ar) {
        this.ar.set(ar);
    }

    public int getNyelv_id() {
        return nyelv_id.get();
    }


    public void setNyelv_id(int nyelv_id) {
        this.nyelv_id.set(nyelv_id);
    }

    public void copyTo(Aruk  aruk){
        aruk.id.set(this.getId());
        aruk.szerzo_id.set(this.getSzerzo_id());
        aruk.kiadas_eve.set(this.getKiadas_eve());
        aruk.kiado_id.set(this.getKiadas_eve());
        aruk.leiras.set(this.getLeiras());
        aruk.cim.set(this.getCim());
        aruk.ar.set(this.getAr());
        aruk.nyelv_id.set(this.getNyelv_id());
    }
}



package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Nyelvek {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty nyelv = new SimpleStringProperty();

    public Nyelvek(int id, String nyelv) {
        this.id.set(id);
        this.nyelv.set(nyelv);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getNyelv() {
        return nyelv.get();
    }

    public StringProperty nyelvProperty() {
        return nyelv;
    }

    public void setNyelv(String nyelv) {
        this.nyelv.set(nyelv);
    }

    public void copyTo(Nyelvek nyelvek) {
        nyelvek.id.set(this.getId());
        nyelvek.nyelv.set(this.getNyelv());
    }

    public Nyelvek() {
    }
}

package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Rendelestetelek {

    private IntegerProperty rendeles_id = new SimpleIntegerProperty();
    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty db = new SimpleIntegerProperty();
    private IntegerProperty ar = new SimpleIntegerProperty();

    public Rendelestetelek(int rendeles_id, int aru_id, int db, int ar) {
        this.rendeles_id.set(rendeles_id);
        this.aru_id.set(aru_id);
        this.db.set(db);
        this.ar.set(ar);
    }

    public int getRendeles_id() {
        return rendeles_id.get();
    }

    public IntegerProperty rendeles_idProperty() {
        return rendeles_id;
    }

    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty dbProperty() {
        return db;
    }

    public IntegerProperty arProperty() {
        return ar;
    }

    public void setAr(Integer ar) {
        this.ar.set(ar);
    }

    public void setRendeles_id(int rendeles_id) {
        this.rendeles_id.set(rendeles_id);
    }

    public int getAru_id() {
        return aru_id.get();
    }

    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getDb() {
        return db.get();
    }

    public void setDb(int db) {
        this.db.set(db);
    }

    public int getAr() {
        return ar.get();
    }

    public void setAr(int ar) {
        this.ar.set(ar);
    }

    public void copyTo(Rendelestetelek rendelestetelek) {
        rendelestetelek.rendeles_id.set(this.getRendeles_id());
        rendelestetelek.aru_id.set(this.getAru_id());
        rendelestetelek.db.set(this.getDb());
        rendelestetelek.ar.set(this.getAr());
    }

    public Rendelestetelek() {
    }
}

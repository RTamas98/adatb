package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Film {
    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty hossz = new SimpleIntegerProperty();

    public Film(int aru_id, int hossz) {
        this.aru_id.set(aru_id);
        this.hossz.set(hossz);
    }

    public int getAru_id() {
        return aru_id.get();
    }


    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getHossz() {
        return hossz.get();
    }


    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty hosszProperty() {
        return hossz;
    }

    public void setAru_id(Integer aru_id) {
        this.aru_id.set(aru_id);
    }

    public void setHossz(Integer hossz) {
        this.hossz.set(hossz);
    }


    public void setHossz(int hossz) {
        this.hossz.set(hossz);
    }

    public void copyTo(Film film) {
        film.aru_id.set(this.getAru_id());
        film.hossz.set(this.getHossz());
    }

    public Film() {
    }
}

package hu.dbalapu.projekt.Konyvesbolt.model;


import javafx.beans.property.*;

public class Konyvek {

    private IntegerProperty aru_id = new SimpleIntegerProperty();
    private IntegerProperty meret = new SimpleIntegerProperty();
    private IntegerProperty oldalszam = new SimpleIntegerProperty();
    private IntegerProperty kotes = new SimpleIntegerProperty();

    public Konyvek(int aru_id, int meret, int oldalszam, int kotes) {
        this.aru_id.set(aru_id);
        this.meret.set(meret);
        this.oldalszam.set(oldalszam);
        this.kotes.set(kotes);
    }

    public int getAru_id() {
        return aru_id.get();
    }

    public void setAru_id(int aru_id) {
        this.aru_id.set(aru_id);
    }

    public int getMeret() {
        return meret.get();
    }


    public void setMeret(int meret) {
        this.meret.set(meret);
    }

    public int getOldalszam() {
        return oldalszam.get();
    }


    public void setOldalszam(int oldalszam) {
        this.oldalszam.set(oldalszam);
    }

    public int getKotes() {
        return kotes.get();
    }

    public IntegerProperty aru_idProperty() {
        return aru_id;
    }

    public IntegerProperty meretProperty() {
        return meret;
    }

    public IntegerProperty oldalszamProperty() {
        return oldalszam;
    }

    public IntegerProperty kotesProperty() {
        return kotes;
    }

    public void setAru_id(Integer aru_id) {
        this.aru_id.set(aru_id);
    }

    public void setMeret(Integer meret) {
        this.meret.set(meret);
    }

    public void setOldalszam(Integer oldalszam) {
        this.oldalszam.set(oldalszam);
    }

    public void setKotes(Integer kotes) {
        this.kotes.set(kotes);
    }


    public void setKotes(int kotes) {
        this.kotes.set(kotes);
    }

    public void copyTo(Konyvek konyvek) {
        konyvek.aru_id.set(this.getAru_id());
        konyvek.kotes.set(this.getKotes());
        konyvek.meret.set(this.getMeret());
        konyvek.oldalszam.set(this.getOldalszam());
    }

    public Konyvek() {
    }
}

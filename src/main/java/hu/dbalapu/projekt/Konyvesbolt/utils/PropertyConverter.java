package hu.dbalapu.projekt.Konyvesbolt.utils;

import javafx.util.converter.NumberStringConverter;

public class PropertyConverter extends NumberStringConverter {
    private static PropertyConverter SINGLE_INSTANCE = null;

    public PropertyConverter() {
    }

    public static PropertyConverter getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new PropertyConverter();
        }
        return SINGLE_INSTANCE;
    }

    @Override
    public String toString(Number object) {
        return object == null ? "" : object.toString();
    }

    @Override
    public Number fromString(String string) {
        if (string == null) {
            return 0;
        }
        else {
            try {
                return Integer.parseInt(string);
            }
            catch (NumberFormatException ex) {
                return 0;
            }
        }
    }
}
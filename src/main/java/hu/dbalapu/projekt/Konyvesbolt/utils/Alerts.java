package hu.dbalapu.projekt.Konyvesbolt.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class Alerts {
    public static final String STATUS_SUCCESS = "Sikeres";
    public static final String STATUS_FAILURE = "Sikertelen";

    public static final String ACTION_ADD = "beszúrás";
    public static final String ACTION_MODIFY = "módosítás";
    public static final String ACTION_DELETE = "törlés";
    public static final String ACTION_LIST = "listázás";
    public static final String ACTION_SEARCH = "keresés";

    public static final ButtonType OK_BUTTON = new ButtonType("OK");
    public static final ButtonType CANCEL_BUTTON = new ButtonType("Mégse");

    private static Alert setTitle(Alert alert) {
        switch (alert.alertTypeProperty().get()) {
            case CONFIRMATION:
                alert.setTitle("Megerősítés");
                break;
            case INFORMATION:
                alert.setTitle("Információ");
                break;
            case WARNING:
                alert.setTitle("Figyelmeztetés");
                break;
            case ERROR:
                alert.setTitle("Hiba");
                break;
            default:
                break;
        }

        return alert;
    }

    public static void alert(String message, AlertType alertType) {
        Alert alert = new Alert(alertType);
        setTitle(alert);

        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static void alert(String status, String action, AlertType alertType) {
        Alert alert = new Alert(alertType);
        setTitle(alert);

        alert.setHeaderText(null);
        alert.setContentText(status + " " + action + "!");
        alert.showAndWait();
    }

    public static Alert buttonedAlert(String message, AlertType alertType) {
        Alert alert = new Alert(alertType, message, OK_BUTTON, CANCEL_BUTTON);
        setTitle(alert);

        alert.setHeaderText(null);
        alert.setContentText(message);

        return alert;
    }
}
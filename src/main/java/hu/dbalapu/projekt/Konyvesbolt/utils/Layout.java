package hu.dbalapu.projekt.Konyvesbolt.utils;

import java.io.IOException;

import hu.dbalapu.projekt.Konyvesbolt.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;

public class Layout {
    public static FXMLLoader createLoader(String fxml) throws IOException {
        return new FXMLLoader(App.class.getResource(fxml + ".fxml"));
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void switchToStage(String fxml, ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Scene scene = new Scene(loadFXML(fxml));

        stage.setTitle("Könyvesbolt");
        stage.setScene(scene);
        stage.show();

        Stage currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        currentStage.close();
    }

    public static void closeStage(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    public static void switchToStage(String fxml, MenuBar menuBar) throws IOException {
        Stage stage = new Stage();
        Scene scene = new Scene(Layout.loadFXML(fxml));

        stage.setTitle("Könyvesbolt");
        stage.setScene(scene);
        stage.show();

        Stage currentStage = (Stage) menuBar.getScene().getWindow();
        currentStage.close();
    }

    public static void closeStage(MenuBar menuBar) {
        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();
    }
}

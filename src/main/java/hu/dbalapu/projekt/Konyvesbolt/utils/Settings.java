package hu.dbalapu.projekt.Konyvesbolt.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Settings {
    public static String URL;
    public static String USERNAME;
    public static String PASSWORD;

    public static void loadDriver() throws ClassNotFoundException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
    }

    public static void setSettings(String host, String port, String sid, String username, String password) {
        URL = "jdbc:oracle:thin:@"+host+":"+port+":"+sid;
        USERNAME = username;
        PASSWORD = password;
    }

    public static void testConnection() throws SQLException {
        Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        if (conn != null) {
            conn.close();
        }
    }
}
package hu.dbalapu.projekt.Konyvesbolt;

import java.io.IOException;

import hu.dbalapu.projekt.Konyvesbolt.utils.Layout;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



/**
 * JavaFX App
 */
public class App extends Application {

    private static Stage stage;
    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(Layout.loadFXML("/hu.dbalapu.projekt.Konyvesbolt/Login"));
        stage.setTitle("Könyvesbolt");
        stage.setScene(scene);
        stage.show();



        /*App.stage = stage;
        App.loadFXML("/hu.dbalapu.projekt.Konyvesbolt/Login.fxml").getController();

        stage.show();

         */

    }

    /*public static FXMLLoader loadFXML(String fxml){
        FXMLLoader loader = new FXMLLoader(App.class.getResource(fxml));
        Scene scene = null;

        try {
            Parent root = loader.load();
            scene = new Scene(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(scene);
        return loader;
    }

     */

    public static void main(String[] args) {
        launch();
    }
}
package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Aruk;

import java.util.List;

public interface ArukDAO {

    public boolean addAruk(Aruk aruk);

    public boolean modifyAruk(Aruk aruk);

    public boolean deleteAruk(Aruk aruk);

    public List<Aruk> listAruk();

    public List<Aruk> searchAruk(String attribute, String query);
}

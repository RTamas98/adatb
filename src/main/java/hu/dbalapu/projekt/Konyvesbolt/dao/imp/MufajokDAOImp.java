package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.MufajokDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Konyvek;
import hu.dbalapu.projekt.Konyvesbolt.model.Mufajok;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MufajokDAOImp implements MufajokDAO {

    private static final String ADD_MUFAJOK = "INSERT INTO MUFAJOK (ID, MUFAJ_NEVE,SZULO_MUFAJ_ID,MELYSEGI_SZINT) VALUES (?, ?,?,? )";
    private static final String MODIFY_MUFAJOK= "UPDATE MUFAJOK SET ID = ?, MUFAJ_NEVE = ?, SZULO_MUFAJ_ID = ? , MELYSEGI_SZINT = ?";
    private static final String DELETE_MUFAJOK = "DELETE FROM MUFAJOK WHERE ID = ?";
    private static final String LIST_MUFAJOK= "SELECT * FROM MUFAJOK";
    private static final String SEARCH_MUFAJOK = "SELECT * FROM MUFAJOK WHERE attribute = ?";

    public MufajokDAOImp(){}

    @Override
    public boolean addMufajok(Mufajok mufajok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_MUFAJOK)) {
            st.setInt(1, mufajok.getId());
            st.setString(2, mufajok.getMufaj_neve());
            st.setInt(3, mufajok.getSzulo_mufaj_id());
            st.setInt(4, mufajok.getMelysegi_szint());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyMufajok(Mufajok mufajok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_MUFAJOK)) {
            st.setInt(1, mufajok.getId());
            st.setString(2, mufajok.getMufaj_neve());
            st.setInt(3, mufajok.getSzulo_mufaj_id());
            st.setInt(4, mufajok.getMelysegi_szint());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteMufajok(Mufajok mufajok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_MUFAJOK)) {
            st.setInt(1, mufajok.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Mufajok> listMufajok() {
        List<Mufajok> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_MUFAJOK);

            while (rs.next()) {
                Mufajok mufajok = new Mufajok(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4));
                result.add(mufajok);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Mufajok> searchMufajok(String attribute, String query) {
        List<Mufajok> result = new ArrayList<>();

        String preparedString = SEARCH_MUFAJOK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Mufajok mufajok = new Mufajok(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4));
            result.add(mufajok);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

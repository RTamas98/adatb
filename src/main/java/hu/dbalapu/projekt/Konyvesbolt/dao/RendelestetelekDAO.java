package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Rendelestetelek;

import java.util.List;

public interface RendelestetelekDAO {

    public boolean addRendelestetelek(Rendelestetelek rendelestetelek);

    public boolean modifyRendelestetelek(Rendelestetelek rendelestetelek);

    public boolean deleteRendelestetelek(Rendelestetelek rendelestetelek);

    public List<Rendelestetelek> listRendelestetelek();

    public List<Rendelestetelek> searchRendelestetelek(String attribute, String query);
}

package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Zene;
import java.util.List;

public interface ZeneDAO {

    public boolean addZene(Zene zene);

    public boolean modifyZene(Zene zene);

    public boolean deleteZene(Zene zene);

    public List<Zene> listZene();

    public List<Zene> searchZene(String attribute, String query);
}

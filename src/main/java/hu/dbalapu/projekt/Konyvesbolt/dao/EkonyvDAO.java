package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Ekonyv;

import java.util.List;

public interface EkonyvDAO {

    public boolean addEkonyv(Ekonyv ekonyv);

    public boolean modifyEkonyv(Ekonyv ekonyv);

    public boolean deleteEkonyv(Ekonyv ekonyv);

    public List<Ekonyv> listEkonyv();

    public List<Ekonyv> searchEkonyv(String attribute, String query);
}

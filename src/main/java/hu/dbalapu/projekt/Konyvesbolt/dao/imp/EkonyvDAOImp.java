package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.EkonyvDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruk;
import hu.dbalapu.projekt.Konyvesbolt.model.Ekonyv;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EkonyvDAOImp implements EkonyvDAO {


    private static final String ADD_EKONYV = "INSERT INTO EKONYV  (ARU_ID, MERET, OLDALSZAM, KITERJESZTES) VALUES (?, ?, ? ,?  )";
    private static final String MODIFY_EKONYV  = "UPDATE EKONYV  SET ARU_ID = ?, MERET = ?, OLDALSZAM = ?, KITERJESZTES = ? ";
    private static final String DELETE_EKONYV  = "DELETE FROM EKONYV  WHERE ARU_ID = ?";
    private static final String LIST_EKONYV = "SELECT * FROM EKONYV ";
    private static final String SEARCH_EKONYV  = "SELECT * FROM EKONYV  WHERE attribute = ?";

    public EkonyvDAOImp(){}

    @Override
    public boolean addEkonyv(Ekonyv ekonyv) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_EKONYV)) {
            st.setInt(1, ekonyv.getAru_id());
            st.setInt(2, ekonyv.getMeret());
            st.setInt(3, ekonyv.getOldalszam());
            st.setString(4, ekonyv.getKiterjesztes());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyEkonyv(Ekonyv ekonyv) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_EKONYV)) {
            st.setInt(1, ekonyv.getAru_id());
            st.setInt(2, ekonyv.getMeret());
            st.setInt(3, ekonyv.getOldalszam());
            st.setString(4, ekonyv.getKiterjesztes());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteEkonyv(Ekonyv ekonyv) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_EKONYV)) {
            st.setInt(1, ekonyv.getAru_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Ekonyv> listEkonyv() {
        List<Ekonyv> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_EKONYV);

            while (rs.next()) {
                Ekonyv ekonyv = new Ekonyv(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getString(4));
                result.add(ekonyv);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Ekonyv> searchEkonyv(String attribute, String query) {
        List<Ekonyv> result = new ArrayList<>();

        String preparedString = SEARCH_EKONYV.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Ekonyv ekonyv = new Ekonyv(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getString(4));
            result.add(ekonyv);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

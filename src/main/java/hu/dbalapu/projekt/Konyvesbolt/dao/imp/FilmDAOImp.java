package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.FilmDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Felhasznalok;
import hu.dbalapu.projekt.Konyvesbolt.model.Film;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FilmDAOImp implements FilmDAO {

    private static final String ADD_FILM = "INSERT INTO FILM (ARU_ID, HOSSZ) VALUES (?, ? )";
    private static final String MODIFY_FILM = "UPDATE FILM SET ARU_ID = ?, HOSSZ = ?";
    private static final String DELETE_FILM = "DELETE FROM FILM WHERE ARU_ID = ?";
    private static final String LIST_FILM = "SELECT * FROM FILM";
    private static final String SEARCH_FILM = "SELECT * FROM FILM WHERE attribute = ?";

    public FilmDAOImp(){}

    @Override
    public boolean addFilm(Film film) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_FILM)) {
            st.setInt(1, film.getAru_id());
            st.setInt(2, film.getHossz());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyFilm(Film film) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_FILM)) {
            st.setInt(1, film.getAru_id());
            st.setInt(2, film.getHossz());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteFilm(Film film) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_FILM)) {
            st.setInt(1, film.getAru_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Film> listFilm() {
        List<Film> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_FILM);

            while (rs.next()) {
                Film film = new Film(rs.getInt(1),rs.getInt(2));
                result.add(film);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Film> searchFilm(String attribute, String query) {
        List<Film> result = new ArrayList<>();

        String preparedString = SEARCH_FILM.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Film film = new Film(rs.getInt(1),rs.getInt(2));
            result.add(film);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.NyilvantartasDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Mufajok;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyilvantartas;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class NyilvantartasDAOImp implements NyilvantartasDAO {

    private static final String ADD_NYILVANTARTASOK = "INSERT INTO NYILVANTARTAS (ID, ARUHAZ_ID, ARU_ID, DB) VALUES (?, ?, ?, ?)";
    private static final String MODIFY_NYILVANTARTASOK= "UPDATE NYILVANTARTAS SET ID = ?, ARUHAZ_ID = ?, ARU_ID = ? , DB = ?";
    private static final String DELETE_NYILVANTARTASOK = "DELETE FROM NYILVANTARTAS WHERE ID = ?";
    private static final String LIST_NYILVANTARTASOK= "SELECT * FROM NYILVANTARTAS";
    private static final String SEARCH_NYILVANTARTASOK = "SELECT * FROM NYILVANTARTAS WHERE attribute = ?";


    public NyilvantartasDAOImp(){}

    @Override
    public boolean addNyilvantartas(Nyilvantartas nyilvantartas) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_NYILVANTARTASOK)) {
            st.setInt(1, nyilvantartas.getAru_id());
            st.setInt(2, nyilvantartas.getAruhaz_id());
            st.setInt(3, nyilvantartas.getDb());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyNyilvantartas(Nyilvantartas nyilvantartas) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_NYILVANTARTASOK)) {
            st.setInt(1, nyilvantartas.getAruhaz_id());
            st.setInt(2, nyilvantartas.getAru_id());
            st.setInt(3, nyilvantartas.getDb());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteNyilvantartas(Nyilvantartas nyilvantartas) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_NYILVANTARTASOK)) {
            st.setInt(1, nyilvantartas.getAruhaz_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Nyilvantartas> listNyilvantartas() {
        List<Nyilvantartas> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_NYILVANTARTASOK);

            while (rs.next()) {
                Nyilvantartas nyilvantartasok = new Nyilvantartas(rs.getInt(1),rs.getInt(2),rs.getInt(3));
                result.add(nyilvantartasok);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Nyilvantartas> searchNyilvantartas(String attribute, String query) {
        List<Nyilvantartas> result = new ArrayList<>();

        String preparedString = SEARCH_NYILVANTARTASOK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Nyilvantartas nyilvantartasok = new Nyilvantartas(rs.getInt(1),rs.getInt(2),rs.getInt(3));
            result.add(nyilvantartasok);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

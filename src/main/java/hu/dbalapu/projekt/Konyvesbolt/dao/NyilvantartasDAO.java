package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Nyilvantartas;

import java.util.List;

public interface NyilvantartasDAO {

        public boolean addNyilvantartas(Nyilvantartas nyilvantartas);

        public boolean modifyNyilvantartas(Nyilvantartas nyilvantartas);

        public boolean deleteNyilvantartas(Nyilvantartas nyilvantartas);

        public List<Nyilvantartas> listNyilvantartas();

        public List<Nyilvantartas> searchNyilvantartas(String attribute, String query);
}

package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Aruhazak;

import java.util.List;

public interface AruhazakDAO {

    public boolean addAruhazak(Aruhazak aruhazak);

    public boolean modifyAruhazak(Aruhazak aruhazak);

    public boolean deleteAruhazak(Aruhazak aruhazak);

    public List<Aruhazak> listAruhazak();

    public List<Aruhazak> searchAruhazak(String attribute, String query);
}

package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.ArumufajDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruk;
import hu.dbalapu.projekt.Konyvesbolt.model.Arumufaj;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArumufajDAOImp implements ArumufajDAO {
    private static final String ADD_ARUMUFAJOK = "INSERT INTO ARUMUFAJ (ARU_ID, MUFAJ_ID) VALUES (?, ?)";
    private static final String MODIFY_ARUMUFAJOK = "UPDATE ARUMUFAJ SET  ARU_ID = ?, MUFAJ_ID = ?";
    private static final String DELETE_ARUMUFAJ = "DELETE FROM ARUMUFAJ WHERE ARU_ID = ?";
    private static final String LIST_ARUMUFAJOK = "";
    private static final String SEARCH_ARUMUFAJOK = "";


    public ArumufajDAOImp() {
    }

    @Override
    public boolean addArumufaj(Arumufaj arumufaj) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_ARUMUFAJOK)) {
            st.setInt(1, arumufaj.getAru_id());
            st.setInt(2, arumufaj.getMufaj_id());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyArumufaj(Arumufaj arumufaj) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_ARUMUFAJOK)) {
            st.setInt(1, arumufaj.getAru_id());
            st.setInt(2, arumufaj.getMufaj_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteArumufaj(Arumufaj arumufaj) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_ARUMUFAJ)) {
            st.setInt(1, arumufaj.getMufaj_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Arumufaj> listArumufaj() {
        List<Arumufaj> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_ARUMUFAJOK);

            while (rs.next()) {
                Arumufaj arumufajok = new Arumufaj(rs.getInt(1), rs.getInt(2));
                result.add(arumufajok);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Arumufaj> searchArumufaj(String attribute, String query) {
        List<Arumufaj> result = new ArrayList<>();

        String preparedString = SEARCH_ARUMUFAJOK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Arumufaj arumufajok = new Arumufaj(rs.getInt(1),rs.getInt(2));
            result.add(arumufajok);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

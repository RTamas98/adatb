package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Felhasznalok;

import java.util.List;

public interface FelhasznalokDAO {

    public boolean addFelhasznalok(Felhasznalok felhasznalok);

    public boolean modifyFelhasznalok(Felhasznalok felhasznalok);

    public boolean deleteFelhasznalok(Felhasznalok felhasznalok);

    public List<Felhasznalok> listFelhasznalok();

    public List<Felhasznalok> searchFelhasznalok(String attribute, String query);
}

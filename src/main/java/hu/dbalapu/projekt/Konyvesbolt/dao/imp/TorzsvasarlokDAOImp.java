package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.TorzsvasarlokDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelestetelek;
import hu.dbalapu.projekt.Konyvesbolt.model.Torzsvasarlok;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TorzsvasarlokDAOImp implements TorzsvasarlokDAO {

    private static final String ADD_TORZSVASARLO = "INSERT INTO TORZSVASARLO (ID, FELHASZNALO_ID, KEDVEZMENY) VALUES (?, ?, ?)";
    private static final String MODIFY_TORZSVASARLO= "UPDATE TORZSVASARLO SET ID, FELHASZNALO_ID = ?, KEDVEZMENY = ?";
    private static final String DELETE_TORZSVASARLO = "DELETE FROM TORZSVASARLO WHERE RENDELES_ID = ?";
    private static final String LIST_TORZSVASARLO = "SELECT * FROM TORZSVASARLO";
    private static final String SEARCH_TORZSVASARLO = "SELECT * FROM TORZSVASARLO WHERE attribute = ?";


    public TorzsvasarlokDAOImp(){}

    @Override
    public boolean addTorzsvasarlok(Torzsvasarlok torzsvasarlok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_TORZSVASARLO)) {
            st.setInt(1, torzsvasarlok.getFelhasznalo_id());
            st.setInt(2, torzsvasarlok.getKedvezmeny());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyTorzsvasarlok(Torzsvasarlok torzsvasarlok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_TORZSVASARLO)) {
            st.setInt(1, torzsvasarlok.getFelhasznalo_id());
            st.setInt(2, torzsvasarlok.getKedvezmeny());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteTorzsvasarlok(Torzsvasarlok torzsvasarlok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_TORZSVASARLO)) {
            st.setInt(1, torzsvasarlok.getFelhasznalo_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Torzsvasarlok> listTorzsvasarlok() {
        List<Torzsvasarlok> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_TORZSVASARLO);

            while (rs.next()) {
                Torzsvasarlok torzsvasarlok = new Torzsvasarlok(rs.getInt(1),rs.getInt(2));
                result.add(torzsvasarlok);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Torzsvasarlok> searchTorzsvasarlok(String attribute, String query) {
        List<Torzsvasarlok> result = new ArrayList<>();

        String preparedString = SEARCH_TORZSVASARLO.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Torzsvasarlok torzsvasarlok = new Torzsvasarlok(rs.getInt(1),rs.getInt(2));
            result.add(torzsvasarlok);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

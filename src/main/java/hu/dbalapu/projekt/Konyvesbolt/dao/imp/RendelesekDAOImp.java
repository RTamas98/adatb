package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.RendelesekDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyilvantartas;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelesek;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RendelesekDAOImp implements RendelesekDAO {

    private static final String ADD_RENDELESEK = "INSERT INTO RENDELESEK (ID, FELHASZNALO_ID, ARUHAZ_ID,KISZALLITVA,DATUM) VALUES (?, ?,?,?,? )";
    private static final String MODIFY_RENDELESEK= "UPDATE RENDELESEK SET ID = ?, FELHASZNALO_ID = ?, ARUHAZ_ID = ?,KISZALLITVA=?,DATUM=?";
    private static final String DELETE_RENDELESEK = "DELETE FROM RENDELESEK WHERE ID = ?";
    private static final String LIST_RENDELESEK =  "SELECT * FROM RENDELESEK";
    private static final String SEARCH_RENDELESEK = "SELECT * FROM RENDELESEK WHERE attribute = ?";

    public RendelesekDAOImp(){}

    @Override
    public boolean addRendelesek(Rendelesek rendelesek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_RENDELESEK)) {
            st.setInt(1, rendelesek.getId());
            st.setInt(2, rendelesek.getFelhasznalo_id());
            st.setInt(3, rendelesek.getAruhaz_id());
            st.setInt(4, rendelesek.getKiszallitva());
            st.setString(5, rendelesek.getDatum());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyRendelesek(Rendelesek rendelesek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_RENDELESEK)) {
            st.setInt(1, rendelesek.getId());
            st.setInt(2, rendelesek.getFelhasznalo_id());
            st.setInt(3, rendelesek.getAruhaz_id());
            st.setInt(4, rendelesek.getKiszallitva());
            st.setString(5, rendelesek.getDatum());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteRendelesek(Rendelesek rendelesek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_RENDELESEK)) {
            st.setInt(1, rendelesek.getAruhaz_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Rendelesek> listRendelesek() {
        List<Rendelesek> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_RENDELESEK);

            while (rs.next()) {
                Rendelesek rendelesek = new Rendelesek(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5));
                result.add(rendelesek);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Rendelesek> searchRendelesek(String attribute, String query) {
        List<Rendelesek> result = new ArrayList<>();

        String preparedString = SEARCH_RENDELESEK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Rendelesek rendelesek = new Rendelesek(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5));
            result.add(rendelesek);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

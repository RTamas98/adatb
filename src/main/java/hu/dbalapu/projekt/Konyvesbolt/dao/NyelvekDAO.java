package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Nyelvek;

import java.util.List;

public interface NyelvekDAO {

    public boolean addNyelvek(Nyelvek nyelvek);

    public boolean modifyNyelvek(Nyelvek nyelvek);

    public boolean deleteNyelvek(Nyelvek nyelvek);

    public List<Nyelvek> listNyelvek();

    public List<Nyelvek> searchNyelvek(String attribute, String query);
}

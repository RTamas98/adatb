package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.SzerzoDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelesek;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelestetelek;
import hu.dbalapu.projekt.Konyvesbolt.model.Szerzo;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SzerzoDAOImp implements SzerzoDAO {

    private static final String ADD_SZERZO = "INSERT INTO SZERZO (ID, NEV) VALUES (?, ? )";
    private static final String MODIFY_SZERZO= "UPDATE SZERZO SET ID = ?, NEV = ?";
    private static final String DELETE_SZERZO = "DELETE FROM SZERZO WHERE ID = ?";
    private static final String LIST_SZERZO =  "SELECT * FROM SZERZO";
    private static final String SEARCH_SZERZO = "SELECT * FROM SZERZO WHERE attribute = ?";

    public SzerzoDAOImp(){}

    @Override
    public boolean addSzerzo(Szerzo szerzo) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_SZERZO)) {
            st.setInt(1, szerzo.getId());
            st.setString(2, szerzo.getNev());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifySzerzo(Szerzo szerzo) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_SZERZO)) {
            st.setInt(1, szerzo.getId());
            st.setString(2, szerzo.getNev());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteSzerzo(Szerzo szerzo) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_SZERZO)) {
            st.setInt(1, szerzo.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Szerzo> listSzerzo() {
        List<Szerzo> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_SZERZO);

            while (rs.next()) {
                Szerzo rendelesek = new Szerzo(rs.getInt(1),rs.getString(2));
                result.add(rendelesek);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Szerzo> searchSzerzo(String attribute, String query) {
        List<Szerzo> result = new ArrayList<>();

        String preparedString = SEARCH_SZERZO.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Szerzo szerzo = new Szerzo(rs.getInt(1),rs.getString(2));
            result.add(szerzo);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

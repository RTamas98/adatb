package hu.dbalapu.projekt.Konyvesbolt.dao.imp;


import hu.dbalapu.projekt.Konyvesbolt.model.Aruhazak;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;
import hu.dbalapu.projekt.Konyvesbolt.dao.AruhazakDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AruhazakDAOImp implements AruhazakDAO{
    private static final String ADD_ARUHAZAK = "INSERT INTO ARUHAZAK (ID, CIM, TELEFON) VALUES (?, ?, ?)";
    private static final String MODIFY_ARUHAZAK = "UPDATE ARUHAZAK SET ID = ?, CIM = ?, TELEFON = ?";
    private static final String DELETE_ARUHAZAK = "DELETE FROM ARUHAZAK WHERE ID = ?";
    private static final String LIST_ARUHAZAK = "SELECT * FROM ARUHAZAK";
    private static final String SEARCH_ARUHAZAK = "SELECT * FROM ARUHAZAK WHERE attribute = ?";

    public AruhazakDAOImp(){}

    @Override
    public boolean addAruhazak(Aruhazak aruhazak){
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_ARUHAZAK)) {
            st.setInt(1, aruhazak.getId());
            st.setString(2, aruhazak.getCim());
            st.setString(3, aruhazak.getTelefon());
            if (st.executeUpdate() == 1) {
                return true;
            }
        }
        catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyAruhazak(Aruhazak aruhazak) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_ARUHAZAK)) {
            st.setInt(1, aruhazak.getId());
            st.setString(2, aruhazak.getCim());
            st.setString(3, aruhazak.getTelefon());

            if (st.executeUpdate() == 1) {
                return true;
            }
        }
        catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteAruhazak(Aruhazak aruhazak) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_ARUHAZAK)) {
            st.setInt(1, aruhazak.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        }
        catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Aruhazak> listAruhazak() {
        List<Aruhazak> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_ARUHAZAK);

            while (rs.next()) {
                Aruhazak aruhazak = new Aruhazak(rs.getInt(1),rs.getString(2),rs.getString(3));
                result.add(aruhazak);
            }
        }
        catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Aruhazak> searchAruhazak(String attribute, String query) {
        List<Aruhazak> result = new ArrayList<>();

        String preparedString = SEARCH_ARUHAZAK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Aruhazak aruhazak = new Aruhazak(rs.getInt(1),rs.getString(2),rs.getString(3));
            result.add(aruhazak);
        }
        catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }


}

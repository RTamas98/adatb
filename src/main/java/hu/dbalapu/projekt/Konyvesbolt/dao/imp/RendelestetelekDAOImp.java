package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.RendelestetelekDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyilvantartas;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelesek;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelestetelek;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RendelestetelekDAOImp implements RendelestetelekDAO {

    private static final String ADD_RENDELESTETEL = "INSERT INTO RENDELESTETEL (RENDELES_ID, ARU_ID, DB, AR) VALUES (?, ?, ?, ?)";
    private static final String MODIFY_RENDELESTETEL= "UPDATE RENDELESTETEL SET RENDELES_ID = ?, ARU_ID = ?, DB = ? , AR = ?";
    private static final String DELETE_RENDELESTETEL = "DELETE FROM RENDELESTETEL WHERE RENDELES_ID = ?";
    private static final String LIST_RENDELESTETELEK = "SELECT * FROM RENDELESTETEL";
    private static final String SEARCH_RENDELESTETELEK = "SELECT * FROM RENDELESTETEL WHERE attribute = ?";


    public RendelestetelekDAOImp(){}

    @Override
    public boolean addRendelestetelek(Rendelestetelek rendelestetelek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_RENDELESTETEL)) {
            st.setInt(1, rendelestetelek.getRendeles_id());
            st.setInt(2, rendelestetelek.getAru_id());
            st.setInt(3, rendelestetelek.getDb());
            st.setInt(4, rendelestetelek.getAr());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyRendelestetelek(Rendelestetelek rendelestetelek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_RENDELESTETEL)) {
            st.setInt(1, rendelestetelek.getRendeles_id());
            st.setInt(2, rendelestetelek.getAru_id());
            st.setInt(3, rendelestetelek.getDb());
            st.setInt(4, rendelestetelek.getAr());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteRendelestetelek(Rendelestetelek rendelestetelek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_RENDELESTETEL)) {
            st.setInt(1, rendelestetelek.getRendeles_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Rendelestetelek> listRendelestetelek() {
        List<Rendelestetelek> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_RENDELESTETELEK);

            while (rs.next()) {
                Rendelestetelek rendelestetelek = new Rendelestetelek(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4));
                result.add(rendelestetelek);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Rendelestetelek> searchRendelestetelek(String attribute, String query) {
        List<Rendelestetelek> result = new ArrayList<>();

        String preparedString = SEARCH_RENDELESTETELEK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Rendelestetelek rendelestetelek = new Rendelestetelek(rs.getInt(1),rs.getInt(2),rs.getInt(3), rs.getInt(4));
            result.add(rendelestetelek);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

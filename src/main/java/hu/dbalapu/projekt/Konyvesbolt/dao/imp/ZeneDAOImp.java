package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.ZeneDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Szerzo;
import hu.dbalapu.projekt.Konyvesbolt.model.Zene;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ZeneDAOImp implements ZeneDAO {


    private static final String ADD_ZENE = "INSERT INTO ZENE (ARU_ID, HOSSZ) VALUES (?, ? )";
    private static final String MODIFY_ZENE = "UPDATE ZENE SET ARU_ID = ?, HOSSZ = ?";
    private static final String DELETE_ZENE = "DELETE FROM ZENE WHERE ARU_ID = ?";
    private static final String LIST_ZENE = "SELECT * FROM ZENE";
    private static final String SEARCH_ZENE = "SELECT * FROM ZENE WHERE attribute = ?";


    public ZeneDAOImp() {
    }

    @Override
    public boolean addZene(Zene zene) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_ZENE)) {
            st.setInt(1, zene.getAru_id());
            st.setInt(2, zene.getHossz());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyZene(Zene zene) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_ZENE)) {
            st.setInt(1, zene.getAru_id());
            st.setInt(2, zene.getHossz());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteZene(Zene zene) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_ZENE)) {
            st.setInt(1, zene.getAru_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Zene> listZene() {
        List<Zene> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_ZENE);

            while (rs.next()) {
                Zene zenek = new Zene(rs.getInt(1), rs.getInt(2));
                result.add(zenek);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Zene> searchZene(String attribute, String query) {
        List<Zene> result = new ArrayList<>();

        String preparedString = SEARCH_ZENE.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Zene zene = new Zene(rs.getInt(1), rs.getInt(2));
            result.add(zene);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}


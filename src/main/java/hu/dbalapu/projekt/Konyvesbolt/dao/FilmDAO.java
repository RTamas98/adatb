package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Film;

import java.util.List;

public interface FilmDAO {

    public boolean addFilm(Film film);

    public boolean modifyFilm(Film film);

    public boolean deleteFilm(Film film);

    public List<Film> listFilm();

    public List<Film> searchFilm(String attribute, String query);
}

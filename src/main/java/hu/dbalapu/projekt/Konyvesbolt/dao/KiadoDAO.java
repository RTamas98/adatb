package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Kiado;

    import java.util.List;

public interface KiadoDAO {

    public boolean addKiado(Kiado konyvek);

    public boolean modifyKiado(Kiado konyvek);

    public boolean deleteKiado(Kiado konyvek);

    public List<Kiado> listKiado();

    public List<Kiado> searchKiado(String attribute, String query);
}

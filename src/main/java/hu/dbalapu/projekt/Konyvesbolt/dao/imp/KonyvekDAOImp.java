package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.KonyvekDAO;

import hu.dbalapu.projekt.Konyvesbolt.model.Konyvek;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class KonyvekDAOImp implements KonyvekDAO {

    private static final String ADD_KONYVEK = "INSERT INTO KONYVEK (ARU_ID, MERET,OLDALSZAM,KOTES) VALUES (?, ?,?,? )";
    private static final String MODIFY_KONYVEK= "UPDATE KONYVEK SET ARU_ID = ?, HOSSZ = ?, OLDALSZAM = ? , KOTES = ?";
    private static final String DELETE_KONYVEK = "DELETE FROM KONYVEK WHERE ARU_ID = ?";
    private static final String LIST_KONYVEK= "SELECT * FROM KONYVEK";
    private static final String SEARCH_KONYVEK = "SELECT * FROM KONYVEK WHERE attribute = ?";

    public KonyvekDAOImp(){}

    @Override
    public boolean addKonyvek(Konyvek konyvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_KONYVEK)) {
            st.setInt(1, konyvek.getAru_id());
            st.setInt(2, konyvek.getMeret());
            st.setInt(3, konyvek.getOldalszam());
            st.setInt(4, konyvek.getKotes());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyKonyvek(Konyvek konyvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_KONYVEK)) {
            st.setInt(1, konyvek.getAru_id());
            st.setInt(2, konyvek.getMeret());
            st.setInt(3, konyvek.getOldalszam());
            st.setInt(4, konyvek.getKotes());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteKonyvek(Konyvek konyvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_KONYVEK)) {
            st.setInt(1, konyvek.getAru_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Konyvek> listKonyvek() {
        List<Konyvek> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_KONYVEK);

            while (rs.next()) {
                Konyvek konyv = new Konyvek(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4));
                result.add(konyv);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Konyvek> searchKonyvek(String attribute, String query) {
        List<Konyvek> result = new ArrayList<>();

        String preparedString = SEARCH_KONYVEK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Konyvek konyv = new Konyvek(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4));
            result.add(konyv);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

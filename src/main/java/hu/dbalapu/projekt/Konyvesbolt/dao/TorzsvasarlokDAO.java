package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Torzsvasarlok;

import java.util.List;

public interface TorzsvasarlokDAO {

    public boolean addTorzsvasarlok(Torzsvasarlok torzsvasarlok);

    public boolean modifyTorzsvasarlok(Torzsvasarlok torzsvasarlok);

    public boolean deleteTorzsvasarlok(Torzsvasarlok torzsvasarlok);

    public List<Torzsvasarlok> listTorzsvasarlok();

    public List<Torzsvasarlok> searchTorzsvasarlok(String attribute, String query);

}

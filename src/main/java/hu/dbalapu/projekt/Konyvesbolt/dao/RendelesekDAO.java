package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Rendelesek;

import java.util.List;

    public interface RendelesekDAO {

    public boolean addRendelesek(Rendelesek rendelesek);

    public boolean modifyRendelesek(Rendelesek rendelesek);

    public boolean deleteRendelesek(Rendelesek rendelesek);

    public List<Rendelesek> listRendelesek();

    public List<Rendelesek> searchRendelesek(String attribute, String query);
}

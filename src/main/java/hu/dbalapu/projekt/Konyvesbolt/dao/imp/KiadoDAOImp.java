package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.KiadoDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruk;
import hu.dbalapu.projekt.Konyvesbolt.model.Kiado;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class KiadoDAOImp implements KiadoDAO {

    private static final String ADD_KIADO = "INSERT INTO KIADO (ID, NEV, CIM) VALUES (?, ?, ?)";
    private static final String MODIFY_KIADO = "UPDATE KIADO SET ID = ?, CIM = ?, NEV = ?";
    private static final String DELETE_KIADO = "DELETE KIADO WHERE ID = ?";
    private static final String LIST_KIADOK = "SELECT * FROM KIADO";
    private static final String SEARCH_KIADOK = "SELECT * FROM KIADO WHERE attribute = ?";


    public KiadoDAOImp(){

    }

    @Override
    public boolean addKiado(Kiado konyvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_KIADO)) {
            st.setInt(1, konyvek.getId());
            st.setString(2, konyvek.getCim());
            st.setString(3, konyvek.getNev());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyKiado(Kiado konyvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_KIADO)) {
            st.setInt(1, konyvek.getId());
            st.setString(2, konyvek.getCim());
            st.setString(3, konyvek.getNev());


            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteKiado(Kiado konyvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_KIADO)) {
            st.setInt(1, konyvek.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Kiado> listKiado() {
        List<Kiado> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_KIADOK);

            while (rs.next()) {
                Kiado kiadok = new Kiado(rs.getInt(1), rs.getString(2), rs.getString(3));
                result.add(kiadok);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Kiado> searchKiado(String attribute, String query) {
        List<Kiado> result = new ArrayList<>();

        String preparedString = SEARCH_KIADOK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Kiado kiadok = new Kiado(rs.getInt(1),rs.getString(2),rs.getString(3));
            result.add(kiadok);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

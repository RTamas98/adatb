package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Mufajok;

import java.util.List;

public interface MufajokDAO {

    public boolean addMufajok(Mufajok mufajok);

    public boolean modifyMufajok(Mufajok mufajok);

    public boolean deleteMufajok(Mufajok mufajok);

    public List<Mufajok> listMufajok();

    public List<Mufajok> searchMufajok(String attribute, String query);
}

package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Konyvek;

import java.util.List;

public interface KonyvekDAO {

    public boolean addKonyvek(Konyvek konyvek);

    public boolean modifyKonyvek(Konyvek konyvek);

    public boolean deleteKonyvek(Konyvek konyvek);

    public List<Konyvek> listKonyvek();

    public List<Konyvek> searchKonyvek(String attribute, String query);
}

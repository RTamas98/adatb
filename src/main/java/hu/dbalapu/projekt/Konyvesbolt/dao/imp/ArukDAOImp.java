package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.ArukDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruhazak;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruk;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArukDAOImp implements ArukDAO {

    private static final String ADD_ARUK = "INSERT INTO ARUK (ID, SZERZO_ID, KIADO_ID, KIADAS_EVE,LEIRAS,CIM,AR,NYELV_ID) VALUES (?, ?, ? ,? , ?, ?, ? ,?  )";
    private static final String MODIFY_ARUK = "UPDATE ARUK SET ID = ?, SZERZO_ID = ?, KIADO_ID = ?, KIADAS_EVE = ?, LEIRAS = ?, CIM = ? , AR = ? , NYELV_ID = ? ";
    private static final String DELETE_ARUK = "DELETE FROM ARUK WHERE ID = ?";
    private static final String LIST_ARUK = "SELECT * FROM ARUK";
    private static final String SEARCH_ARUK = "SELECT * FROM ARUK WHERE attribute = ?";

    public ArukDAOImp() {
    }

    @Override
    public boolean addAruk(Aruk aruk) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_ARUK)) {
            st.setInt(1, aruk.getId());
            st.setInt(2, aruk.getSzerzo_id());
            st.setInt(3, aruk.getKiado_id());
            st.setInt(4, aruk.getKiadas_eve());
            st.setString(5, aruk.getLeiras());
            st.setString(6, aruk.getCim());
            st.setInt(7, aruk.getAr());
            st.setInt(8, aruk.getNyelv_id());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyAruk(Aruk aruk) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_ARUK)) {
            st.setInt(1, aruk.getId());
            st.setInt(2, aruk.getSzerzo_id());
            st.setInt(3, aruk.getKiado_id());
            st.setInt(4, aruk.getKiadas_eve());
            st.setString(5, aruk.getLeiras());
            st.setString(6, aruk.getCim());
            st.setInt(7, aruk.getAr());
            st.setInt(8, aruk.getNyelv_id());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteAruk(Aruk aruk) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_ARUK)) {
            st.setInt(1, aruk.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Aruk> listAruk() {
        List<Aruk> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_ARUK);

            while (rs.next()) {
                Aruk aruk = new Aruk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4), rs.getString(5), rs.getString(6),rs.getInt(7),rs.getInt(8));
                result.add(aruk);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Aruk> searchAruk(String attribute, String query) {
        List<Aruk> result = new ArrayList<>();

        String preparedString = SEARCH_ARUK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Aruk aruk = new Aruk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4), rs.getString(5), rs.getString(6),rs.getInt(7),rs.getInt(8));
            result.add(aruk);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

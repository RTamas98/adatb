package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Arumufaj;

import java.util.List;

public interface ArumufajDAO {

    public boolean addArumufaj(Arumufaj arumufaj);

    public boolean modifyArumufaj(Arumufaj arumufaj);

    public boolean deleteArumufaj(Arumufaj arumufaj);

    public List<Arumufaj> listArumufaj();

    public List<Arumufaj> searchArumufaj(String attribute, String query);
}

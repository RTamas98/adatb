package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.NyelvekDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Mufajok;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyelvek;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class NyelvekDAOImp implements NyelvekDAO {

    private static final String ADD_NYELVEK = "INSERT INTO NYELVEK (ID, NYELV) VALUES (?, ? )";
    private static final String MODIFY_NYELVEK= "UPDATE NYELVEK SET ID = ?, NYELV = ?";
    private static final String DELETE_NYELVEK = "DELETE FROM NYELVEK WHERE ID = ?";
    private static final String LIST_NYELVEK= "SELECT * FROM NYELVEK";
    private static final String SEARCH_NYELVEK = "SELECT * FROM NYELVEK WHERE attribute = ?";

    public NyelvekDAOImp(){}

    @Override
    public boolean addNyelvek(Nyelvek nyelvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_NYELVEK)) {
            st.setInt(1, nyelvek.getId());
            st.setString(2, nyelvek.getNyelv());
            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyNyelvek(Nyelvek nyelvek) {

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_NYELVEK)) {
            st.setInt(1, nyelvek.getId());
            st.setString(2, nyelvek.getNyelv());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteNyelvek(Nyelvek nyelvek) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_NYELVEK)) {
            st.setInt(1, nyelvek.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Nyelvek> listNyelvek() {
        List<Nyelvek> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_NYELVEK);

            while (rs.next()) {
                Nyelvek nyelvek = new Nyelvek(rs.getInt(1),rs.getString(2));
                result.add(nyelvek);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;

    }

    @Override
    public List<Nyelvek> searchNyelvek(String attribute, String query) {
        List<Nyelvek> result = new ArrayList<>();

        String preparedString = SEARCH_NYELVEK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Nyelvek nyelvek = new Nyelvek(rs.getInt(1),rs.getString(2));
            result.add(nyelvek);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

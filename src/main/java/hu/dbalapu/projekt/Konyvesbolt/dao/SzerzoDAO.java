package hu.dbalapu.projekt.Konyvesbolt.dao;

import hu.dbalapu.projekt.Konyvesbolt.model.Szerzo;

import java.util.List;

public interface SzerzoDAO {

    public boolean addSzerzo(Szerzo szerzo);

    public boolean modifySzerzo(Szerzo szerzo);

    public boolean deleteSzerzo(Szerzo szerzo);

    public List<Szerzo> listSzerzo();

    public List<Szerzo> searchSzerzo(String attribute, String query);



}

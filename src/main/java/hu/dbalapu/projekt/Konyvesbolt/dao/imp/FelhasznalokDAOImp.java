package hu.dbalapu.projekt.Konyvesbolt.dao.imp;

import hu.dbalapu.projekt.Konyvesbolt.dao.FelhasznalokDAO;
import hu.dbalapu.projekt.Konyvesbolt.model.Felhasznalok;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FelhasznalokDAOImp implements FelhasznalokDAO {

    private static final String ADD_FELHASZNALOK = "INSERT INTO FELHASZNALOK (ID, FELHASZNALONEV, JELSZO, EMAIL,NEV,TELEFON,IRSZ,VAROS,UTCA,HAZSZAM,JOGOSULTSAG) VALUES (?, ?, ? ,? , ?, ?, ? ,? , ?, ? ,? )";
    private static final String MODIFY_FELHASZNALOK = "UPDATE FELHASZNALOK SET ID = ?, FELHASZNALONEV = ?, JELSZO = ?, EMAIL = ?, NEV = ?, TELEFON = ? , IRSZ = ? , VAROS = ?, UTCA = ? , HAZSZAM = ? , JOGOSULTSAG = ? ";
    private static final String DELETE_FELHASZNALOK = "DELETE FROM FELHASZNALOK WHERE ID = ?";
    private static final String LIST_FELHASZNALOK = "SELECT * FROM FELHASZNALOK";
    private static final String SEARCH_FELHASZNALOK = "SELECT * FROM FELHASZNALOK WHERE attribute = ?";

    public FelhasznalokDAOImp(){}

    @Override
    public boolean addFelhasznalok(Felhasznalok felhasznalok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(ADD_FELHASZNALOK)) {
            st.setInt(1, felhasznalok.getId());
            st.setString(2, felhasznalok.getFelhasznalonev());
            st.setString(3, felhasznalok.getJelszo());
            st.setString(4, felhasznalok.getEmail());
            st.setString(5, felhasznalok.getNev());
            st.setString(6, felhasznalok.getTelefon());
            st.setString(7, felhasznalok.getIrsz());
            st.setString(8, felhasznalok.getVaros());
            st.setString(9, felhasznalok.getUtca());
            st.setInt(10, felhasznalok.getHazszam());
            st.setInt(11, felhasznalok.getJogosultsag());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean modifyFelhasznalok(Felhasznalok felhasznalok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(MODIFY_FELHASZNALOK)) {
            st.setInt(1, felhasznalok.getId());
            st.setString(2, felhasznalok.getFelhasznalonev());
            st.setString(3, felhasznalok.getJelszo());
            st.setString(4, felhasznalok.getEmail());
            st.setString(5, felhasznalok.getNev());
            st.setString(6, felhasznalok.getTelefon());
            st.setString(7, felhasznalok.getIrsz());
            st.setString(8, felhasznalok.getVaros());
            st.setString(9, felhasznalok.getUtca());
            st.setInt(10, felhasznalok.getHazszam());
            st.setInt(11, felhasznalok.getJogosultsag());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteFelhasznalok(Felhasznalok felhasznalok) {
        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(DELETE_FELHASZNALOK)) {
            st.setInt(1, felhasznalok.getId());

            if (st.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Felhasznalok> listFelhasznalok() {
        List<Felhasznalok> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery(LIST_FELHASZNALOK);

            while (rs.next()) {
                Felhasznalok felhasznalok = new Felhasznalok(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getInt(10),rs.getInt(11));
                result.add(felhasznalok);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Felhasznalok> searchFelhasznalok(String attribute, String query) {
        List<Felhasznalok> result = new ArrayList<>();

        String preparedString = SEARCH_FELHASZNALOK.replace("attribute", attribute);

        try (Connection conn = DriverManager.getConnection(Settings.URL, Settings.USERNAME, Settings.PASSWORD);
             PreparedStatement st = conn.prepareStatement(preparedString)) {
            st.setString(1, query);

            ResultSet rs = st.executeQuery();

            rs.next();

            Felhasznalok felhasznalok = new Felhasznalok(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getInt(10),rs.getInt(11));
            result.add(felhasznalok);
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return result;
    }
}

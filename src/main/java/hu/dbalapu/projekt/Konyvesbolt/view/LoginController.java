package hu.dbalapu.projekt.Konyvesbolt.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import hu.dbalapu.projekt.Konyvesbolt.utils.Alerts;
import hu.dbalapu.projekt.Konyvesbolt.utils.Layout;
import hu.dbalapu.projekt.Konyvesbolt.utils.Settings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController implements Initializable {
    @FXML
    private TextField hostTF;
    @FXML
    private TextField portTF;
    @FXML
    private TextField sidTF;
    @FXML
    private TextField usernameTF;
    @FXML
    private PasswordField passwordPF;

    public LoginController() {
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.hostTF.setText("localhost");
        this.portTF.setText("1521");
        this.sidTF.setText("xe");
    }

    @FXML
    private void okAction(ActionEvent event) {
        try {
            Settings.loadDriver();
        }
        catch (ClassNotFoundException exc) {
            Alerts.alert("Nem található a driver!", AlertType.ERROR);
            exc.printStackTrace();
            return;
        }

        String host = this.hostTF.getText();
        String port = this.portTF.getText();
        String sid = this.sidTF.getText();
        String username = this.usernameTF.getText();
        String password = this.passwordPF.getText();

        Settings.setSettings(host, port, sid, username, password);

        try {
            Settings.testConnection();
        }
        catch (SQLException exc) {

            Alerts.alert("Helytelen bejelentkezési adatok!", AlertType.ERROR);
            exc.printStackTrace();
            return;
        }
        
        try {
            Layout.switchToStage("/hu.dbalapu.projekt.Konyvesbolt/Trivial", event);
        }
        catch (IOException exc) {
            Alerts.alert("Nem található az FXML fájl!", AlertType.ERROR);
            exc.printStackTrace();
        }
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        Layout.closeStage(event);
    }
}

package hu.dbalapu.projekt.Konyvesbolt.view;

import javafx.fxml.FXML;

public interface TableController {
    @FXML
    public void addItem();

    @FXML
    public void refreshTable();
}

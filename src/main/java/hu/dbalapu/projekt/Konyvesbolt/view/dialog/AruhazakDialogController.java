package hu.dbalapu.projekt.Konyvesbolt.view.dialog;

import hu.dbalapu.projekt.Konyvesbolt.controller.AruhazakController;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruhazak;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import hu.dbalapu.projekt.Konyvesbolt.model.Zene;
import hu.dbalapu.projekt.Konyvesbolt.utils.Alerts;
import hu.dbalapu.projekt.Konyvesbolt.utils.Layout;
import hu.dbalapu.projekt.Konyvesbolt.utils.PropertyConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class AruhazakDialogController implements Initializable {

    private Aruhazak aruhazak = new Aruhazak();

    public boolean add;

//    @FXML
//    private TextField idTF;
    @FXML
    private TextField cimTF;
    @FXML
    private TextField telefonTF;

    public AruhazakDialogController() {
        aruhazak.copyTo(this.aruhazak);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //this.idTF.textProperty().bindBidirectional(this.aruhazak.idProperty(), PropertyConverter.getInstance());
        this.cimTF.textProperty().bindBidirectional(this.aruhazak.cimProperty());
        this.telefonTF.textProperty().bindBidirectional(this.aruhazak.telefonProperty());
    }

    @FXML
    private void okAction(ActionEvent event) {
        if (this.add) {
            if (AruhazakController.getInstance().addAruhazak(this.aruhazak)) {
                Alerts.alert(Alerts.STATUS_SUCCESS, Alerts.ACTION_ADD, AlertType.INFORMATION);
                Layout.closeStage(event);
            } else {
                Alerts.alert(Alerts.STATUS_FAILURE, Alerts.ACTION_ADD, AlertType.ERROR);
            }
        } else {
            if (AruhazakController.getInstance().modifyAruhazak(this.aruhazak)) {
                Alerts.alert(Alerts.STATUS_SUCCESS, Alerts.ACTION_ADD, AlertType.INFORMATION);
                Layout.closeStage(event);
            } else {
                Alerts.alert(Alerts.STATUS_FAILURE, Alerts.ACTION_ADD, AlertType.ERROR);
            }
        }
    }

    @FXML
    private void cancelAction(ActionEvent event){
        Layout.closeStage(event);
    }
}

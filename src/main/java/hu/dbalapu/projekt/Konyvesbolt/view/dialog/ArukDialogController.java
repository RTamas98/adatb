package hu.dbalapu.projekt.Konyvesbolt.view.dialog;

import hu.dbalapu.projekt.Konyvesbolt.controller.ArukController;
import hu.dbalapu.projekt.Konyvesbolt.controller.KiadoController;
import hu.dbalapu.projekt.Konyvesbolt.controller.NyelvekController;
import hu.dbalapu.projekt.Konyvesbolt.controller.SzerzoController;
import hu.dbalapu.projekt.Konyvesbolt.model.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import hu.dbalapu.projekt.Konyvesbolt.utils.Alerts;
import hu.dbalapu.projekt.Konyvesbolt.utils.Layout;
import hu.dbalapu.projekt.Konyvesbolt.utils.PropertyConverter;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ArukDialogController implements Initializable {

    public boolean add;

    private Aruk aruk = new Aruk();

    @FXML
    private ComboBox<Integer> szerzoCB;

    @FXML
    private ComboBox<Integer> kiadoCB;

    @FXML
    private TextField kiadasevCB;

    @FXML
    private TextField leirasTF;

    @FXML
    private TextField cimTF;

    @FXML
    private TextField arTF;

    @FXML
    private ComboBox<Integer> nyelvCB;

    public ArukDialogController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        List<Szerzo> szerzoList = SzerzoController.getInstance().listSzerzo();
        List<Kiado> kiadoList = KiadoController.getInstance().listKonyvek();
        List<Nyelvek> nyelvekList = NyelvekController.getInstance().listNyelvek();

        List<Integer> szerzoIdList = new ArrayList<>();
        List<Integer> kiadoIdList = new ArrayList<>();
        List<Integer> nyelvekIdList = new ArrayList<>();

        for (Szerzo szerzo : szerzoList) {
            szerzoIdList.add(szerzo.getId());
        }

        for (Kiado kiado : kiadoList) {
            kiadoIdList.add(kiado.getId());
        }

        for (Nyelvek nyelvek : nyelvekList) {
            nyelvekIdList.add(nyelvek.getId());
        }

        this.szerzoCB.setItems(FXCollections.observableList(szerzoIdList));
        this.kiadoCB.setItems(FXCollections.observableList(kiadoIdList));
        this.nyelvCB.setItems(FXCollections.observableList(nyelvekIdList));


        this.szerzoCB.valueProperty().bindBidirectional(this.aruk.szerzo_idProperty().asObject());
        this.kiadoCB.valueProperty().bindBidirectional(this.aruk.kiado_idProperty().asObject());
        this.nyelvCB.valueProperty().bindBidirectional(this.aruk.nyelv_idProperty().asObject());
        this.kiadasevCB.textProperty().bindBidirectional(this.aruk.kiadas_eveProperty(), PropertyConverter.getInstance());


//TODO: MEGCSINALNI A FELETTE LEVO SORT
        this.leirasTF.textProperty().bindBidirectional(this.aruk.leirasProperty());
        this.cimTF.textProperty().bindBidirectional(this.aruk.cimProperty());
        this.arTF.textProperty().bindBidirectional(this.aruk.arProperty(), PropertyConverter.getInstance());

    }

    @FXML
    private void okAction(ActionEvent event) {
        if (this.add) {
            if (ArukController.getInstance().addAruk(this.aruk)) {
                Alerts.alert(Alerts.STATUS_SUCCESS, Alerts.ACTION_ADD, AlertType.INFORMATION);
                Layout.closeStage(event);
            } else {
                Alerts.alert(Alerts.STATUS_FAILURE, Alerts.ACTION_ADD, AlertType.ERROR);
            }
        } else {
            if (ArukController.getInstance().modifyAruk(this.aruk)) {
                Alerts.alert(Alerts.STATUS_SUCCESS, Alerts.ACTION_ADD, AlertType.INFORMATION);
                Layout.closeStage(event);
            } else {
                Alerts.alert(Alerts.STATUS_FAILURE, Alerts.ACTION_ADD, AlertType.ERROR);
            }
        }
    }
    @FXML
    private void cancelAction(ActionEvent event) {
        Layout.closeStage(event);
    }
}

package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.KiadoDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.KonyvekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.KiadoDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.KonyvekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Kiado;
import hu.dbalapu.projekt.Konyvesbolt.model.Konyvek;

public class KiadoController {

    private static KiadoController SINGLE_INSTANCE = null;

    private KiadoDAO dao = new KiadoDAOImp();

    public KiadoController() {
    }

    public static KiadoController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new KiadoController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addKiado(Kiado kiado) {
        return dao.addKiado(kiado);
    }

    public boolean modifyKiado(Kiado kiado) {
        return dao.modifyKiado(kiado);
    }

    public boolean deleteKiado(Kiado kiado) {
        return dao.deleteKiado(kiado);
    }

    public List<Kiado> listKonyvek() {
        return dao.listKiado();
    }

    public List<Kiado> searchKiado(String attribute, String query) {
        return dao.searchKiado(attribute, query);
    }
}

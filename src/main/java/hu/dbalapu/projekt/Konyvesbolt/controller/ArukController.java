package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.ArukDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.ArukDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruk;

public class ArukController {

    private static ArukController SINGLE_INSTANCE = null;

    private ArukDAO dao = new ArukDAOImp();

    public ArukController() {
    }

    public static ArukController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new ArukController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addAruk(Aruk aruk) {
        return dao.addAruk(aruk);
    }

    public boolean modifyAruk(Aruk aruk) {
        return dao.modifyAruk(aruk);
    }

    public boolean deleteAruk(Aruk aruk) {
        return dao.deleteAruk(aruk);
    }

    public List<Aruk> listArumufaj() {
        return dao.listAruk();
    }

    public List<Aruk> searchAruk(String attribute, String query) {
        return dao.searchAruk(attribute, query);
    }
}

package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.RendelesekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.RendelestetelekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.RendelesekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.RendelestetelekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelesek;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelestetelek;

public class RendelesekController {

    private static RendelesekController SINGLE_INSTANCE = null;

    private RendelesekDAO dao = new RendelesekDAOImp();

    public RendelesekController() {
    }

    public static RendelesekController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new RendelesekController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addRendelesek(Rendelesek rendelesek) {
        return dao.addRendelesek(rendelesek);
    }

    public boolean modifyRendelesek(Rendelesek rendelesek) {
        return dao.modifyRendelesek(rendelesek);
    }

    public boolean deleteRendelesek(Rendelesek rendelesek) {
        return dao.deleteRendelesek(rendelesek);
    }

    public List<Rendelesek> listRendelesek() {
        return dao.listRendelesek();
    }

    public List<Rendelesek> searchRendelesek(String attribute, String query) {
        return dao.searchRendelesek(attribute, query);
    }
}

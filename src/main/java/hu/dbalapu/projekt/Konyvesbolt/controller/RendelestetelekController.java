package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.RendelestetelekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.SzerzoDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.RendelestetelekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.SzerzoDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelestetelek;
import hu.dbalapu.projekt.Konyvesbolt.model.Szerzo;

public class RendelestetelekController {

    private static RendelestetelekController SINGLE_INSTANCE = null;

    private RendelestetelekDAO dao = new RendelestetelekDAOImp();

    public RendelestetelekController() {
    }

    public static RendelestetelekController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new RendelestetelekController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addRendelestetelek(Rendelestetelek rendelestetelek) {
        return dao.addRendelestetelek(rendelestetelek);
    }

    public boolean modifyRendelestetelek(Rendelestetelek rendelestetelek) {
        return dao.modifyRendelestetelek(rendelestetelek);
    }

    public boolean deleteRendelestetelek(Rendelestetelek rendelestetelek) {
        return dao.deleteRendelestetelek(rendelestetelek);
    }

    public List<Rendelestetelek> listRendelestetelek() {
        return dao.listRendelestetelek();
    }

    public List<Rendelestetelek> searchRendelestetelek(String attribute, String query) {
        return dao.searchRendelestetelek(attribute, query);
    }
}

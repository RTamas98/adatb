package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.EkonyvDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.EkonyvDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Ekonyv;

public class EkonyvController {

    private static EkonyvController SINGLE_INSTANCE = null;

    private EkonyvDAO dao = new EkonyvDAOImp();

    public EkonyvController() {
    }

    public static EkonyvController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new EkonyvController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addEkonyv(Ekonyv ekonyv) {
        return dao.addEkonyv(ekonyv);
    }

    public boolean modifyEkonyv(Ekonyv ekonyv) {
        return dao.modifyEkonyv(ekonyv);
    }

    public boolean deleteEkonyv(Ekonyv ekonyv) {
        return dao.deleteEkonyv(ekonyv);
    }

    public List<Ekonyv> listEkonyv() {
        return dao.listEkonyv();
    }

    public List<Ekonyv> searchEkonyv(String attribute, String query) {
        return dao.searchEkonyv(attribute, query);
    }
}

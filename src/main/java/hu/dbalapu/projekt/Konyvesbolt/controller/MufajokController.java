package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.MufajokDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.NyelvekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.MufajokDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.NyelvekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Mufajok;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyelvek;

public class MufajokController {

    private static MufajokController SINGLE_INSTANCE = null;

    private MufajokDAO dao = new MufajokDAOImp();

    public MufajokController() {
    }

    public static MufajokController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new MufajokController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addMufajok(Mufajok mufajok) {
        return dao.addMufajok(mufajok);
    }

    public boolean modifyMufajok(Mufajok mufajok) {
        return dao.modifyMufajok(mufajok);
    }

    public boolean deleteMufajok(Mufajok mufajok) {
        return dao.deleteMufajok(mufajok);
    }

    public List<Mufajok> listMufajok() {
        return dao.listMufajok();
    }

    public List<Mufajok> searchMufajok(String attribute, String query) {
        return dao.searchMufajok(attribute, query);
    }
}

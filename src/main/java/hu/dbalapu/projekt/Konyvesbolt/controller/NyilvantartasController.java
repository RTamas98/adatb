package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.NyilvantartasDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.RendelesekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.NyilvantartasDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.RendelesekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyilvantartas;
import hu.dbalapu.projekt.Konyvesbolt.model.Rendelesek;

public class NyilvantartasController {

    private static NyilvantartasController SINGLE_INSTANCE = null;

    private NyilvantartasDAO dao = new NyilvantartasDAOImp();

    public NyilvantartasController() {
    }

    public static NyilvantartasController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new NyilvantartasController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addNyilvantartas(Nyilvantartas nyilvantartas) {
        return dao.addNyilvantartas(nyilvantartas);
    }

    public boolean modifyNyilvantartas(Nyilvantartas nyilvantartas) {
        return dao.modifyNyilvantartas(nyilvantartas);
    }

    public boolean deleteNyilvantartas(Nyilvantartas nyilvantartas) {
        return dao.deleteNyilvantartas(nyilvantartas);
    }

    public List<Nyilvantartas> listNyilvantartas() {
        return dao.listNyilvantartas();
    }

    public List<Nyilvantartas> searchNyilvantartas(String attribute, String query) {
        return dao.searchNyilvantartas(attribute, query);
    }
}

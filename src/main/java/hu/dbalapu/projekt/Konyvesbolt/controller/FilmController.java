package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.FilmDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.KiadoDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.FilmDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.KiadoDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Film;

public class FilmController {

    private static FilmController SINGLE_INSTANCE = null;

    private FilmDAO dao = new FilmDAOImp();

    public FilmController() {
    }

    public static FilmController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new FilmController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addFilm(Film film) {
        return dao.addFilm(film);
    }

    public boolean modifyFilm(Film film) {
        return dao.modifyFilm(film);
    }

    public boolean deleteFilm(Film film) {
        return dao.deleteFilm(film);
    }

    public List<Film> listFilm() {
        return dao.listFilm();
    }

    public List<Film> searchFilm(String attribute, String query) {
        return dao.searchFilm(attribute, query);
    }
}

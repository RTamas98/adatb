package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.TorzsvasarlokDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.TorzsvasarlokDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Torzsvasarlok;

public class TorzsvasarlokController {

    private static TorzsvasarlokController SINGLE_INSTANCE = null;

    private TorzsvasarlokDAO dao = new TorzsvasarlokDAOImp();

    public TorzsvasarlokController() {
    }

    public static TorzsvasarlokController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new TorzsvasarlokController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addTorzsvasarlok(Torzsvasarlok torzsvasarlok) {
        return dao.addTorzsvasarlok(torzsvasarlok);
    }

    public boolean modifyTorzsvasarlok(Torzsvasarlok torzsvasarlok) {
        return dao.modifyTorzsvasarlok(torzsvasarlok);
    }

    public boolean deleteTorzsvasarlok(Torzsvasarlok torzsvasarlok) {
        return dao.deleteTorzsvasarlok(torzsvasarlok);
    }

    public List<Torzsvasarlok> listTorzsvasarlok() {
        return dao.listTorzsvasarlok();
    }

    public List<Torzsvasarlok> searchTorzsvasarlok(String attribute, String query) {
        return dao.searchTorzsvasarlok(attribute, query);
    }

}

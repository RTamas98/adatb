package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.NyelvekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.NyilvantartasDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.NyelvekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.NyilvantartasDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyelvek;
import hu.dbalapu.projekt.Konyvesbolt.model.Nyilvantartas;

public class NyelvekController {

    private static NyelvekController SINGLE_INSTANCE = null;

    private NyelvekDAO dao = new NyelvekDAOImp();

    public NyelvekController() {
    }

    public static NyelvekController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new NyelvekController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addNyelvek(Nyelvek nyelvek) {
        return dao.addNyelvek(nyelvek);
    }

    public boolean modifyNyelvek(Nyelvek nyelvek) {
        return dao.modifyNyelvek(nyelvek);
    }

    public boolean deleteNyelvek(Nyelvek nyelvek) {
        return dao.deleteNyelvek(nyelvek);
    }

    public List<Nyelvek> listNyelvek() {
        return dao.listNyelvek();
    }

    public List<Nyelvek> searchNyelvek(String attribute, String query) {
        return dao.searchNyelvek(attribute, query);
    }
}

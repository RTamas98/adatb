package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.FelhasznalokDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.FelhasznalokDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Felhasznalok;

public class FelhasznalokController {

    private static FelhasznalokController SINGLE_INSTANCE = null;

    private FelhasznalokDAO dao = new FelhasznalokDAOImp();

    public FelhasznalokController() {
    }

    public static FelhasznalokController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new FelhasznalokController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addFelhasznalok(Felhasznalok felhasznalok) {
        return dao.addFelhasznalok(felhasznalok);
    }

    public boolean modifyFelhasznalok(Felhasznalok felhasznalok) {
        return dao.modifyFelhasznalok(felhasznalok);
    }

    public boolean deleteFelhasznalok(Felhasznalok felhasznalok) {
        return dao.deleteFelhasznalok(felhasznalok);
    }

    public List<Felhasznalok> listFelhasznalok() {
        return dao.listFelhasznalok();
    }

    public List<Felhasznalok> searchFelhasznalok(String attribute, String query) {
        return dao.searchFelhasznalok(attribute, query);
    }
}

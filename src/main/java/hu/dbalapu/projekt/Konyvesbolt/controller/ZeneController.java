package hu.dbalapu.projekt.Konyvesbolt.controller;

import hu.dbalapu.projekt.Konyvesbolt.dao.ZeneDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.ZeneDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Zene;

import java.util.List;

public class ZeneController {

    private static ZeneController SINGLE_INSTANCE = null;

    private ZeneDAO dao = new ZeneDAOImp();

    public ZeneController() {
    }

    public static ZeneController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new ZeneController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addZene(Zene zene) {
        return dao.addZene(zene);
    }

    public boolean modifyZene(Zene zene) {
        return dao.modifyZene(zene);
    }

    public boolean deleteZene(Zene zene) {
        return dao.deleteZene(zene);
    }

    public List<Zene> listZene() {
        return dao.listZene();
    }

    public List<Zene> searchZene(String attribute, String query) {
        return dao.searchZene(attribute, query);
    }
}

package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.SzerzoDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.TorzsvasarlokDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.SzerzoDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.TorzsvasarlokDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Szerzo;
import hu.dbalapu.projekt.Konyvesbolt.model.Torzsvasarlok;

public class SzerzoController {

    private static SzerzoController SINGLE_INSTANCE = null;

    private SzerzoDAO dao = new SzerzoDAOImp();

    public SzerzoController() {
    }

    public static SzerzoController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new SzerzoController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addSzerzo(Szerzo szerzo) {
        return dao.addSzerzo(szerzo);
    }

    public boolean modifySzerzo(Szerzo szerzo) {
        return dao.modifySzerzo(szerzo);
    }

    public boolean deleteSzerzo(Szerzo szerzo) {
        return dao.deleteSzerzo(szerzo);
    }

    public List<Szerzo> listSzerzo() {
        return dao.listSzerzo();
    }

    public List<Szerzo> searchSzerzo(String attribute, String query) {
        return dao.searchSzerzo(attribute, query);
    }
}

package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.AruhazakDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.AruhazakDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Aruhazak;

public class AruhazakController{

    private static AruhazakController SINGLE_INSTANCE = null;

    private AruhazakDAO dao = new AruhazakDAOImp();

    public AruhazakController() {
    }

    public static AruhazakController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new AruhazakController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addAruhazak(Aruhazak aruhazak) {
        return dao.addAruhazak(aruhazak);
    }

    public boolean modifyAruhazak(Aruhazak aruhazak) {
        return dao.modifyAruhazak(aruhazak);
    }

    public boolean deleteAruhazak(Aruhazak aruhazak) {
        return dao.deleteAruhazak(aruhazak);
    }

    public List<Aruhazak> listAruhazak() {
        return dao.listAruhazak();
    }

    public List<Aruhazak> searchAruhazak(String attribute, String query) {
        return dao.searchAruhazak(attribute, query);
    }
}

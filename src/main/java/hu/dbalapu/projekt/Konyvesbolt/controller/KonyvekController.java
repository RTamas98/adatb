package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.KonyvekDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.MufajokDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.KonyvekDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.MufajokDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Konyvek;
import hu.dbalapu.projekt.Konyvesbolt.model.Mufajok;

public class KonyvekController {

    private static KonyvekController SINGLE_INSTANCE = null;

    private KonyvekDAO dao = new KonyvekDAOImp();

    public KonyvekController() {
    }

    public static KonyvekController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new KonyvekController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addKonyvek(Konyvek konyvek) {
        return dao.addKonyvek(konyvek);
    }

    public boolean modifyKonyvek(Konyvek konyvek) {
        return dao.modifyKonyvek(konyvek);
    }

    public boolean deleteKonyvek(Konyvek konyvek) {
        return dao.deleteKonyvek(konyvek);
    }

    public List<Konyvek> listKonyvek() {
        return dao.listKonyvek();
    }

    public List<Konyvek> searchKonyvek(String attribute, String query) {
        return dao.searchKonyvek(attribute, query);
    }
}

package hu.dbalapu.projekt.Konyvesbolt.controller;

import java.util.List;

import hu.dbalapu.projekt.Konyvesbolt.dao.ArumufajDAO;
import hu.dbalapu.projekt.Konyvesbolt.dao.imp.ArumufajDAOImp;
import hu.dbalapu.projekt.Konyvesbolt.model.Arumufaj;

public class ArumufajController {

    private static ArumufajController SINGLE_INSTANCE = null;

    private ArumufajDAO dao = new ArumufajDAOImp();

    public ArumufajController() {
    }

    public static ArumufajController getInstance() {
        if (SINGLE_INSTANCE == null) {
            SINGLE_INSTANCE = new ArumufajController();
        }
        return SINGLE_INSTANCE;
    }

    public boolean addArumufaj(Arumufaj arumufaj) {
        return dao.addArumufaj(arumufaj);
    }

    public boolean modifyArumufaj(Arumufaj arumufaj) {
        return dao.modifyArumufaj(arumufaj);
    }

    public boolean deleteArumufaj(Arumufaj arumufaj) {
        return dao.deleteArumufaj(arumufaj);
    }

    public List<Arumufaj> listArumufaj() {
        return dao.listArumufaj();
    }

    public List<Arumufaj> searchArumufaj(String attribute, String query) {
        return dao.searchArumufaj(attribute, query);
    }
}
